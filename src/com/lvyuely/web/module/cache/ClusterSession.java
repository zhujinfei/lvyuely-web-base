package com.lvyuely.web.module.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ClusterSession implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private final Map<String, Object> map = new HashMap<String, Object>();

	public void removeAttribute(String key) {
		if (map.containsKey(key)) {
			map.remove(key);
		}
	}

	public void setAttribute(String key, Object value) {
		this.map.put(key, value);
	}

	public Object getAttribute(String key) {
		return this.map.get(key);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
