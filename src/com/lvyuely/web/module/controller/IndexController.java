package com.lvyuely.web.module.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

import com.lvyuely.web.module.cache.ClusterSession;
import com.lvyuely.web.module.cache.ClusterSessionFactory;
import com.lvyuely.web.module.entity.auth.Account;

@Controller
public class IndexController {

	@Autowired
	private ClusterSessionFactory sessionFactory;

	@RequestMapping(value = { "/", "/index" })
	public String index(Model model) {
		ClusterSession session = sessionFactory.create();
		Account account = new Account();
		account.setId(1231l);
		account.setUsername("吕悦");
		session.setAttribute("account", account);
		sessionFactory.store(session);
		model.addAttribute("account", account);
		return "index/index.jsp";
	}
	// @RequestMapping("index")
	// @ResponseBody
	// public Account index() {
	// ClusterSession session = sessionFactory.create();
	// Account account = new Account();
	// account.setId(1231l);
	// account.setUsername("吕悦");
	// session.setAttribute("account", account);
	// sessionFactory.store(session);
	// return account;
	// }
}
