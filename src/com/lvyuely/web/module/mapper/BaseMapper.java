package com.lvyuely.web.module.mapper;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseMapper<K extends Serializable, V> {

	public int save(V vo);

	public int deleteById(K id);

	public int update(V vo);

	public V getById(K id);

	public List<V> listAll();

	public List<V> listByPage(@Param("start") long start, @Param("length") int length);

}
